﻿using System;
using System.IO;
using System.Linq;
using System.Windows.Forms;

namespace ImageSorterBySize
{
    public class File
    {
        public void MoveFilesToFolderByMinSize(string[] fileExtensions, string sourcePath,
            string destinationPath, decimal minSize)
        {
            if (minSize > 0 && !string.IsNullOrEmpty(sourcePath)
                && !string.IsNullOrEmpty(destinationPath))
            {
                var minUserImageSizeInBytes = minSize * 1000;

                try
                {
                    foreach (var file in Directory.EnumerateFiles(sourcePath, "*.*")
                        .Where(path => fileExtensions.Any(ext => ext == Path.GetExtension(path).ToLower())))
                    {
                        var fileInfo = new FileInfo(file);

                        if (fileInfo.Length > minUserImageSizeInBytes)
                        {
                            var destinationFileName = destinationPath + $@"\{Path.GetFileName(file)}";
                            
                            System.IO.File.Move(file, destinationFileName);
                        }
                    }

                    MessageBox.Show("Done!", "",
                        MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Error",
                        MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
            {
                MessageBox.Show("Not all fields are filled in!", "Warning",
                        MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }
    }
}
