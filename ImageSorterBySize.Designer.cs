﻿namespace ImageSorterBySize
{
    partial class ImageSorterBySize
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ImageSorterBySize));
            this.InputFolder = new System.Windows.Forms.Label();
            this.InputFolderTextBox = new System.Windows.Forms.TextBox();
            this.OutputFolder = new System.Windows.Forms.Label();
            this.OutputFolderTextBox = new System.Windows.Forms.TextBox();
            this.ImageSize = new System.Windows.Forms.Label();
            this.StartButton = new System.Windows.Forms.Button();
            this.ImageSizeTextBox = new System.Windows.Forms.NumericUpDown();
            ((System.ComponentModel.ISupportInitialize)(this.ImageSizeTextBox)).BeginInit();
            this.SuspendLayout();
            // 
            // InputFolder
            // 
            this.InputFolder.AutoSize = true;
            this.InputFolder.BackColor = System.Drawing.Color.Transparent;
            this.InputFolder.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.InputFolder.Location = new System.Drawing.Point(12, 32);
            this.InputFolder.Name = "InputFolder";
            this.InputFolder.Size = new System.Drawing.Size(125, 23);
            this.InputFolder.TabIndex = 0;
            this.InputFolder.Text = "Input folder";
            this.InputFolder.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // InputFolderTextBox
            // 
            this.InputFolderTextBox.BackColor = System.Drawing.Color.White;
            this.InputFolderTextBox.ForeColor = System.Drawing.Color.White;
            this.InputFolderTextBox.Location = new System.Drawing.Point(163, 32);
            this.InputFolderTextBox.Name = "InputFolderTextBox";
            this.InputFolderTextBox.Size = new System.Drawing.Size(349, 23);
            this.InputFolderTextBox.TabIndex = 1;
            this.InputFolderTextBox.Click += new System.EventHandler(this.InputFolderTextBox_Click);
            // 
            // OutputFolder
            // 
            this.OutputFolder.AutoSize = true;
            this.OutputFolder.BackColor = System.Drawing.Color.Transparent;
            this.OutputFolder.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.OutputFolder.Location = new System.Drawing.Point(12, 81);
            this.OutputFolder.Name = "OutputFolder";
            this.OutputFolder.Size = new System.Drawing.Size(139, 23);
            this.OutputFolder.TabIndex = 2;
            this.OutputFolder.Text = "Output folder";
            this.OutputFolder.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // OutputFolderTextBox
            // 
            this.OutputFolderTextBox.BackColor = System.Drawing.Color.White;
            this.OutputFolderTextBox.ForeColor = System.Drawing.Color.White;
            this.OutputFolderTextBox.Location = new System.Drawing.Point(163, 81);
            this.OutputFolderTextBox.Name = "OutputFolderTextBox";
            this.OutputFolderTextBox.Size = new System.Drawing.Size(349, 23);
            this.OutputFolderTextBox.TabIndex = 3;
            this.OutputFolderTextBox.Click += new System.EventHandler(this.OutputFolderTextBox_Click);
            // 
            // ImageSize
            // 
            this.ImageSize.AutoSize = true;
            this.ImageSize.BackColor = System.Drawing.Color.Transparent;
            this.ImageSize.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.ImageSize.Location = new System.Drawing.Point(12, 133);
            this.ImageSize.Name = "ImageSize";
            this.ImageSize.Size = new System.Drawing.Size(138, 23);
            this.ImageSize.TabIndex = 4;
            this.ImageSize.Text = "Min size (KB)";
            this.ImageSize.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // StartButton
            // 
            this.StartButton.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.StartButton.BackColor = System.Drawing.Color.Transparent;
            this.StartButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.StartButton.FlatAppearance.BorderColor = System.Drawing.Color.RoyalBlue;
            this.StartButton.FlatAppearance.BorderSize = 2;
            this.StartButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.LightSteelBlue;
            this.StartButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LightSteelBlue;
            this.StartButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.StartButton.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.StartButton.ForeColor = System.Drawing.Color.RoyalBlue;
            this.StartButton.Location = new System.Drawing.Point(12, 196);
            this.StartButton.Name = "StartButton";
            this.StartButton.Size = new System.Drawing.Size(400, 45);
            this.StartButton.TabIndex = 6;
            this.StartButton.Text = "Start";
            this.StartButton.UseVisualStyleBackColor = false;
            this.StartButton.Click += new System.EventHandler(this.StartButton_Click);
            // 
            // ImageSizeTextBox
            // 
            this.ImageSizeTextBox.Location = new System.Drawing.Point(163, 133);
            this.ImageSizeTextBox.Maximum = new decimal(new int[] {
            12000,
            0,
            0,
            0});
            this.ImageSizeTextBox.Name = "ImageSizeTextBox";
            this.ImageSizeTextBox.Size = new System.Drawing.Size(104, 23);
            this.ImageSizeTextBox.TabIndex = 7;
            this.ImageSizeTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // ImageSorterBySize
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(524, 271);
            this.Controls.Add(this.ImageSizeTextBox);
            this.Controls.Add(this.StartButton);
            this.Controls.Add(this.ImageSize);
            this.Controls.Add(this.OutputFolderTextBox);
            this.Controls.Add(this.OutputFolder);
            this.Controls.Add(this.InputFolderTextBox);
            this.Controls.Add(this.InputFolder);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ImageSorterBySize";
            this.Padding = new System.Windows.Forms.Padding(0, 15, 0, 15);
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Image Sorter By Size";
            this.Load += new System.EventHandler(this.ImageSorterBySize_Load);
            ((System.ComponentModel.ISupportInitialize)(this.ImageSizeTextBox)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label InputFolder;
        private System.Windows.Forms.TextBox InputFolderTextBox;
        private System.Windows.Forms.Label OutputFolder;
        private System.Windows.Forms.TextBox OutputFolderTextBox;
        private System.Windows.Forms.Label ImageSize;
        private System.Windows.Forms.Button StartButton;
        private System.Windows.Forms.NumericUpDown ImageSizeTextBox;
    }
}

