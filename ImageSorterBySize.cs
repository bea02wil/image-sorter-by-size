﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace ImageSorterBySize
{
    public partial class ImageSorterBySize : Form
    {
        private Folder folder;
        private File file;
        private Designer designer;

        public ImageSorterBySize()
        {
            InitializeComponent();
        }

        private void ImageSorterBySize_Load(object sender, EventArgs e)
        {
            InitializeStartingValues();
        }

        private void InputFolderTextBox_Click(object sender, EventArgs e)
        {
            this.InputFolderTextBox.Text = folder.GetPath();

            designer.MakeInputOfColor(this.InputFolderTextBox, Color.Green);
        }

        private void OutputFolderTextBox_Click(object sender, EventArgs e)
        {
            this.OutputFolderTextBox.Text = folder.GetPath();

            designer.MakeInputOfColor(this.OutputFolderTextBox, Color.Green);
        }

        private void StartButton_Click(object sender, EventArgs e)
        {
            string[] imageExtensions = { ".jpg", ".jpeg", ".png", ".gif" };

            file.MoveFilesToFolderByMinSize(
                imageExtensions,
                this.InputFolderTextBox.Text,
                this.OutputFolderTextBox.Text,
                this.ImageSizeTextBox.Value
               );
        }

        private void InitializeStartingValues()
        {
            this.ImageSizeTextBox.Text = string.Empty;

            this.InputFolderTextBox.ReadOnly = true;
            this.OutputFolderTextBox.ReadOnly = true;

            this.ActiveControl = this.InputFolder;

            this.StartButton.Left = (this.ClientSize.Width - this.StartButton.Width) / 2;

            folder = new Folder();
            file = new File();
            designer = new Designer();
        }
    }
}
