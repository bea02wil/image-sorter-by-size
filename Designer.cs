﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace ImageSorterBySize
{
    public class Designer
    {
        public void MakeInputOfColor(TextBox inputField, Color color)
        {
            if (!string.IsNullOrEmpty(inputField.Text))
            {
                inputField.BackColor = color;
            }
            else
            {
                inputField.BackColor = Color.White;
            }
        }
    }
}
