﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

namespace ImageSorterBySize
{
    public class Folder
    {
        public string GetPath()
        {
            var folderBrowserDialog = new FolderBrowserDialog();
            folderBrowserDialog.Description = "<3";
            folderBrowserDialog.RootFolder = Environment.SpecialFolder.MyComputer;

            if (folderBrowserDialog.ShowDialog() == DialogResult.OK)
            {
                return folderBrowserDialog.SelectedPath;
            }

            return string.Empty;
        }
    }
}
